const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


    let earningsmore_than_$500M = Object.entries(favouritesMovies)
       .filter((currentMovies) => {
        let earnings = currentMovies[1].totalEarnings
        .replace("$","")
        .replace("M","")
        if(learning > 500) {
             return currentMovies;
        }
    },{})

    

    let movieswithmoreThan30scarNomination = earningsmore_than_$500M.
    filter((correntMovies)=>{
     if(currentMovies[1].oscarnominations >3) {
        return currentMovies;

     }},{});


     let LeonardoDicaprio = object.entries(favouritesMovies)
                                    .filter((currentMovies) => {
     if (currentMovie[1].actors.includes("leonardo Dicaprio")) {
        return currentMovie;
     }
    },{});

    let SortedListAccToIMDBRating = object.entries(favouritesMovies)
    .sort((movies, movie2) => {
        let earnings1 = movie1[1].totalEarnings
        .replace("$", "")
        .replace("M", "");

        let earning2 = movie2[1].totalEarnings
        .replace("$", "")
        .replace("M", "");
        if (movie1[1].imdbrating < movie2[1].imdbRating)return
        if (movie1[1].imdbRating == movie2[1].imdbRating) {

        }
    }, {})
